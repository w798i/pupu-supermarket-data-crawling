module javafxPupu{
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.sql;


    opens javafxPupu to javafx.fxml;
    exports javafxPupu;
}