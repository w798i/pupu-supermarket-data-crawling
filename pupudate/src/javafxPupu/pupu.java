package javafxPupu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class pupu extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(PupuController.class.getResource("PupuView.fxml"));
        Scene scene=new Scene(fxmlLoader.load(),800,500);
        primaryStage.setScene(scene);
        primaryStage.setTitle("系统");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
