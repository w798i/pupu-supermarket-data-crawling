/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : shopping

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-04-27 17:32:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pupu
-- ----------------------------
DROP TABLE IF EXISTS `pupu`;
CREATE TABLE `pupu` (
  `name` varchar(255) NOT NULL,
  `spec` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `sub_title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pupu
-- ----------------------------
INSERT INTO `pupu` VALUES ('钙芝奶酪味威化饼干135g', '135g/盒', '6.80', '融入浓郁浓香的奶酪，口味酥脆奶香四溢');
INSERT INTO `pupu` VALUES ('锐澳微醺3度乳酸菌伏特加鸡尾酒330ml/罐', '330ml/罐', '7.90', '温柔奶萌的乳酸菌与热烈奔放的伏特加一拍即合，口感刚刚好');
INSERT INTO `pupu` VALUES ('鲜呆这咸蛋黄海苔卷105g', '105g/罐', '13.90', '源自中国紫菜之乡，层层酥脆，满口回味');
INSERT INTO `pupu` VALUES ('宜又鲜猪排骨（带脊骨）300g±9g', '300g±9g/份', '15.80', '营养丰富，做法多样，肋排3+脊骨7');
INSERT INTO `pupu` VALUES ('Freeplus芙丽芳丝净润洗面（洁面）霜100g', '100g/支', '79.00', '氨基酸系 温和洁面不拔干');
INSERT INTO `pupu` VALUES ('伊利须尽欢醇牛乳酪酪牛乳芝士味冰淇淋75g', '75g/支', '9.90', '优选鲜牛乳添加量≥39%；搭配咸香芝士；选用3.8倍蛋白浓缩乳，口感更醇厚。');
INSERT INTO `pupu` VALUES ('伊利纸盒装巧乐兹巧脆棒冰淇淋75g*5支', '75g*5支/盒', '17.80', '香浓巧克力脆皮，幼滑香草口味冰淇淋');
INSERT INTO `pupu` VALUES ('杉村高品质优食蛋20枚', '20枚/盒', '21.80', '精选优质玉米喂养，颗颗饱满醇香');
