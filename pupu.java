import java.sql.*;
import java.util.Scanner;

public class pupu {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/sys?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";

    static final String USER = "root";
    static final String PASS = "cs2001330";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try{
            Class.forName(JDBC_DRIVER);

            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            System.out.println("实例化Statement对象...");
            stmt = conn.createStatement();
            String sql;
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入要查询的内容：");
            String select = sc.nextLine();
            sql = "SELECT name, spec, price, sub_title FROM pupu WHERE name LIKE '%"+select+"%' OR spec LIKE '%"+select+"%' OR price LIKE '%"+select+"%' OR sub_title LIKE '%"+select+"%'";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                String name  = rs.getString("name");
                String spec = rs.getString("spec");
                String price = rs.getString("price");
                String sub_title = rs.getString("sub_title");

                System.out.print("商品名: " + name);
                System.out.print(", 规格: " + spec);
                System.out.print(", 单价: " + price);
                System.out.println(", 简介: " + sub_title);
            }
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }
}
