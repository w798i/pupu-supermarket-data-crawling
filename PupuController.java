package javafxPupu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;


public class PupuController {

    @FXML
    private TextField tfFilename;
    @FXML
    private TableView<Map> table;
    @FXML
    private TableColumn<Map,String> name;
    @FXML
    private TableColumn<Map,String> spec;
    @FXML
    private TableColumn<Map, Float> price;
    @FXML
    private TableColumn<Map,String> sub_title;
    @FXML
    private Label la;

    private String url="jdbc:mysql://localhost:3306/shopping";
    private String user="root";
    private String password="root";
    private Connection conn= null;

    @FXML
    void initialize() {
        try {
            name.setCellValueFactory(new MapValueFactory<String>("商品名"));
            spec.setCellValueFactory(new MapValueFactory<String>("规格"));
            price.setCellValueFactory(new MapValueFactory<Float>("价格"));
            sub_title.setCellValueFactory(new MapValueFactory<String>("简介"));
            conn = DriverManager.getConnection(url,user,password);
            Statement statement=conn.createStatement();
            ResultSet rs=statement.executeQuery("select * from pupu");
            while (rs.next()){
                HashMap<String, Object> hashMap=new HashMap<>();
                String name  = rs.getString("name");
                String spec = rs.getString("spec");
                float price = rs.getFloat("price");
                String sub = rs.getString("sub_title");
                hashMap.put("商品名",name);
                hashMap.put("规格",spec);
                hashMap.put("价格",price);
                hashMap.put("简介",sub);
                table.getItems().add(hashMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    //下移
    @FXML
    void downClick(ActionEvent event) {
        Map dname=table.getSelectionModel().getSelectedItem();
        if (dname !=null){
            la.setText("");
            try {
                conn = DriverManager.getConnection(url,user,password);
                Statement statement=conn.createStatement();
                statement.executeUpdate("delete from pupu where name='"+dname.get("商品名")+"'");
                table.getItems().clear();
                initialize();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else {
            la.setText("  未选中哦!");
        }
    }

    //上移
    @FXML
    void upClick(ActionEvent event) {
        String sname=tfFilename.getText();
        table.getItems().clear();
        try {
            conn = DriverManager.getConnection(url,user,password);
            Statement statement=conn.createStatement();
            ResultSet rs=statement.executeQuery("select * from pupu where name like'%"+sname+"%'");
            while (rs.next()){
                HashMap<String, Object> hashMap=new HashMap<>();
                String name  = rs.getString("name");
                String spec = rs.getString("spec");
                float price = rs.getFloat("price");
                String sub = rs.getString("sub_title");
                hashMap.put("商品名",name);
                hashMap.put("规格",spec);
                hashMap.put("价格",price);
                hashMap.put("简介",sub);
                table.getItems().add(hashMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

